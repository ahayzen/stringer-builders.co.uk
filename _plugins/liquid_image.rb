# SPDX-FileCopyrightText: 2020 Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

# frozen_string_literal: true

require 'mini_magick'

module Jekyll
  # {% image photo.jpg resize-rule(s) [attr="value"] %}
  #
  # eg {% image photo.jpg original,fill-100x100 alt="drawing" class="left" %}
  #
  # attr can be any html tag, if sizes/srcset is set we'll generate the other images
  # resize rules can provide many of the following
  #  - fill-WxH
  #  - max-WxH
  #  - min-WxH
  #  - width-W
  #  - original
  #
  # there can be either one or two rules, first is for desktop usage and second for mobile
  class ImageTag < Liquid::Tag
    @@to_remove = []

    def self.to_remove
      @@to_remove
    end

    def self.to_remove_reset
      @@to_remove = []
    end

    def initialize(tag_name, text, tokens)
      super
      @text = text.strip
    end

    def make_rule_filename(name, rule)
      # Insert the rule before the file extension
      filename, _, ext = name.rpartition('.')
      filename << '-' << rule << '.' << ext
    end

    def make_image(context)
      # Render any liquid options in the args text given
      text = Liquid::Template.parse(@text).render(context)

      # Process the text into capture sections
      markup = /^(?<src>\S+)\s(?<rule>[a-z0-9\-,]+){1}(?<html_attrs>\s+[\S\s]+){0,1}$/.match(text)
      raise 'Image tag is not in the form {% image photo.png resize-rule [attr="value"] %}' unless markup

      # Read defaults from settings
      site = context.registers[:site]
      assets_dir = site.config['assets_dir'] || 'assets'

      # Markup src
      markup_src = markup[:src]
      # The desktop / fallback src to use in html
      src = File.join(assets_dir, markup_src)
      # The original path not to include in _site
      src_path = File.join(site.source, src)
      # Alternate paths to use in html
      srcset = []

      # Check the original path exists
      raise "Image does not exist: #{markup_src}" unless File.file?(src_path)

      # Process what the rules mean
      rules = markup[:rule].strip.split(',')

      raise "Image has too many rules (should have 1 or 2): #{markup[:rule]}" if rules.size > 2

      (0..(rules.size - 1)).each do |i|
        # Generate the filename for the rule
        rule_filename = make_rule_filename(markup_src, rules[i])
        # Add the rule image to the state files if it doesn't exist (this will generate the resizes image)
        unless site.static_files.any? do |sfile|
                 sfile.instance_of?(ImageFile) && (sfile.name == markup_src) && (sfile.rule == rules[i]) && (sfile.rule_filename == rule_filename)
               end
          site.static_files << ImageFile.new(site, 'assets', markup_src, rules[i], rule_filename)
        end

        # Generate the image path for the rule image
        image_path = File.join(assets_dir, rule_filename)

        # First image is the desktop / fallback image
        # Secondary images are alternate mobile variants
        if i == 0
          src = image_path
        else
          # Create the srcset and sizes for the image
          srcset << (String.new << '/' << image_path)
        end
      end

      # Indicate that we do not need the src file in the resultant static_files
      @@to_remove << src_path

      # Build html markup part
      html_markup = markup[:html_attrs] != '' ? markup[:html_attrs] : ''

      [src, srcset, html_markup]
    end

    def render(context)
      src, srcset, html_markup = make_image(context)

      site = context.registers[:site]
      mobile_css_media_rule = site.config['images_mobile_css_media_rule'] || '(max-width: 1000px)'

      if srcset.size > 0
        String.new << '<picture><source media="' << mobile_css_media_rule << '" srcset="' << srcset[0] << '" /><img src="/' << src << '" ' << html_markup << ' /></picture>'
      else
        String.new << '<img src="/' << src << '" ' << html_markup << ' />'
      end
    end
  end

  class ImageAsSrcTag < ImageTag
    def render(context)
      String.new << '/' << make_image(context)[0]
    end
  end

  class ImageFile < StaticFile
    attr_reader :rule, :rule_filename

    def initialize(site, dir, name, rule, rule_filename)
      super(site, site.source, dir, name, nil)

      # Set relative path, as other methods in StateFile use this
      # for example url and destination, which then cause the conflicting
      # url health checker to think multiple files have the same dest
      # even though we are generating different files
      @relative_path = File.join(*[dir, rule_filename].compact)

      @rule = rule
      @rule_filename = rule_filename
    end

    def make_dimensions(filepath, rule, image)
      # This returns the width, height, and imagicmagick geometry for a given rule
      # http://www.imagemagick.org/script/command-line-processing.php#geometry
      case rule
      when /^fill-(?<width>\d+)x(?<height>\d+)$/
        [$LAST_MATCH_INFO['width'], $LAST_MATCH_INFO['height'],
         $LAST_MATCH_INFO['width'] << 'x' << $LAST_MATCH_INFO['height'] << '^']
      when /^max-(?<width>\d+)x(?<height>\d+)$/
        [$LAST_MATCH_INFO['width'], $LAST_MATCH_INFO['height'],
         $LAST_MATCH_INFO['width'] << 'x' << $LAST_MATCH_INFO['height'] << '>']
      when /^min-(?<width>\d+)x(?<height>\d+)$/
        [$LAST_MATCH_INFO['width'], $LAST_MATCH_INFO['height'],
         $LAST_MATCH_INFO['width'] << 'x' << $LAST_MATCH_INFO['height'] << '^']
      when /^width-(?<width>\d+)$/
        [$LAST_MATCH_INFO['width'], image.height * ($LAST_MATCH_INFO['width'] / image.width),
         $LAST_MATCH_INFO['width']]
      when 'original'
        [image.width, image.height, '100%']
      else
        raise "Image has unknown resize rule: #{rule} for #{filepath}"
      end
    end

    def write(dest)
      # generate the extra image that we need
      src_path = File.join(@site.source, @dir, @name)
      dest_path = File.join(dest, @dir, @rule_filename)

      # Ensure the target directory exists
      FileUtils.mkdir_p(File.dirname(dest_path))

      # If disk cache is enabled then setup directories and vars
      if Cache.disk_cache_enabled
        # Generate cache file path
        cache_path = File.join(@site.cache_dir, 'liquid_image', @dir, @rule_filename)

        # Ensure cache directory exists
        FileUtils.mkdir_p(File.dirname(cache_path))
      end

      # Only run MiniMagick command if one of the following is true
      #  - disk cache is disabled
      #  - cache file does not exist
      #  - src file is newer than cached dest file
      if !Cache.disk_cache_enabled || !File.file?(cache_path) || (File.mtime(cache_path) < File.mtime(src_path))
        image = MiniMagick::Image.open(src_path)

        # Detect the width, height and generate the geometry rule
        width, height, magic_geometry = make_dimensions(@name, @rule, image)

        if @rule.start_with?('fill')
          # Fill needs to resize and crop
          image.combine_options do |i|
            i.auto_orient
            i.resize magic_geometry
            i.gravity 'center'
            i.strip
            i.crop(String.new << width << 'x' << height << '+0+0')
          end
        else
          image.combine_options do |i|
            i.auto_orient
            i.resize magic_geometry
            i.gravity 'center'
            i.strip
          end
        end

        image.write dest_path

        # If disk cache is enabled, cache generated file
        FileUtils.cp(dest_path, cache_path) if Cache.disk_cache_enabled
      elsif Cache.disk_cache_enabled
        # Disk cache is enabled and cache file is valid so copy target file from cache
        FileUtils.cp(cache_path, dest_path)
      end
    end
  end

  # Hook removes any src images that we have resized from static_files list
  Jekyll::Hooks.register :site, :post_render do |site|
    site.static_files.delete_if do |static_file|
      ImageTag.to_remove.include?(static_file.path) && !static_file.instance_of?(ImageFile)
    end

    ImageTag.to_remove_reset
  end
end

Liquid::Template.register_tag('image', Jekyll::ImageTag)

Liquid::Template.register_tag('image_as_src', Jekyll::ImageAsSrcTag)
