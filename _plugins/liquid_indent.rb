# SPDX-FileCopyrightText: 2019, 2020 Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

# frozen_string_literal: true

# Liquid filter that indents newlines by a specified amount
module Jekyll
  module IndentFilter
    def indent(input, spaces)
      # Check if there are any <pre*>*</pre> tags in the content
      # as we don't want to indent anything inside <pre>
      match_data = input.match(%r{(<pre[\S|\s|\R]*>[\S|\s|\R]*</pre>)})

      if !match_data.nil?
        # Split the data on the matches
        # Perform the indent on the split data (outside <pre>)
        # Zip the captures and indented data back together
        # Join the array back into a string
        input
          .split(%r{<pre[\S|\s|\R]*>[\S|\s|\R]*</pre>})
          .map { |s| s.gsub(/\n/, "\n" + (' ' * spaces)) }
          .zip(match_data.captures).flatten.compact
          .join('')
      else
        input.gsub(/\n/, "\n" + (' ' * spaces))
      end
    end
  end
end

Liquid::Template.register_filter(Jekyll::IndentFilter)
