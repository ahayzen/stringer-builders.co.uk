# SPDX-FileCopyrightText: 2019, 2020 Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

# frozen_string_literal: true

module Jekyll
  class Typescript < Generator
    safe true
    priority :low

    def generate(site)
      ts_files = []

      # Find all ts files, remove them from static files  and replace with our own StaticFile
      # implementation that generates cached js files via the write method
      site.static_files.delete_if do |static_file|
        # Check the static file is a typescript file
        next unless static_file.extname == '.ts'

        # Generate a StaticFile for the ts file
        ts_files << TsFile.new(site, static_file.destination_rel_dir, static_file.name)

        # Indicate that we want to remove this entry from the static_files
        true
      end

      # concat new tsfiles with static files
      site.static_files.concat(ts_files)
    end
  end

  class TsFile < StaticFile
    def initialize(site, dir, name)
      super(site, site.source, dir, name, nil)

      @ts_path = File.join(site.source, dir, name)
      @js_relative_path = File.join(dir, @name.sub(/\.ts$/i, '.js'))
    end

    def write(dest)
      # Generate js path
      js_path = File.join(dest, @js_relative_path)

      # Ensure the target directory exists
      FileUtils.mkdir_p(File.join(dest, @dir))

      # If disk cache is enabled then setup directories and vars
      if Cache.disk_cache_enabled
        # Generate cache file path
        cache_base = File.join(@site.cache_dir, 'jekyll_typescript')
        cache_path = File.join(cache_base, @js_relative_path)

        # Ensure cache directory exists
        FileUtils.mkdir_p(File.join(cache_base, @dir))
      end

      # Only run tsc compile command if one of the following is true
      #  - disk cache is disabled
      #  - cache file does not exist
      #  - ts file is newer than cached js file
      if !Cache.disk_cache_enabled || !File.file?(cache_path) || (File.mtime(cache_path) < File.mtime(@ts_path))
        begin
          command = "tsc --inlineSources --outFile #{js_path} --removeComments --sourceMap --strict --target ES6 #{@ts_path}"
          output = `#{command}`

          # Check if the typescript compilation worked
          raise "TypeScript failed for #{@ts_path} with:\n#{output}" if $?.exitstatus != 0

          # If disk cache is enabled, cache js and js map file
          if Cache.disk_cache_enabled
            FileUtils.cp(js_path, cache_path)
            FileUtils.cp(js_path + '.map', cache_path + '.map')
          end
        end
      elsif Cache.disk_cache_enabled
        # Disk cache is enabled and cache file is valid so copy target file from cache
        FileUtils.cp(cache_path, js_path)
        FileUtils.cp(cache_path + '.map', js_path + '.map')
      end
    end
  end
end
