# SPDX-FileCopyrightText: 2020 Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

# frozen_string_literal: true

module Jekyll
  class AnalyticsTag < Liquid::Tag
    GOOGLE_CODE = ''"<!-- Begin Google Analytics -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', '%s', {'storage': 'none'});
ga('set', 'anonymizeIp', true);
ga('send', 'pageview');
</script>
<!-- End Google Analytics -->"''

    def initialize(tag_name, text, tokens)
      super
      @text = text
    end

    def render(context)
      # Extract the google analytics tag from site settings
      site = context.registers[:site]

      if site.config.has_key?('analytics_tags') && site.config['analytics_tags'].has_key?('google')
        google_analytics_tag = site.config['analytics_tags']['google']
      else
        raise "Could not find config key for analytics that matches the following:\nanalytics_tags:\n  google: UA-1234-1"
      end

      # Only render the analytics when in production mode
      if ENV['JEKYLL_ENV'] == 'production'
        (GOOGLE_CODE % google_analytics_tag)
      else
        ''
      end
    end
  end
end

Liquid::Template.register_tag('analytics', Jekyll::AnalyticsTag)
