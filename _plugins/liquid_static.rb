# SPDX-FileCopyrightText: 2020 Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

# frozen_string_literal: true

module Jekyll
  class StaticTag < Liquid::Tag
    def initialize(tag_name, text, tokens)
      super
      @text = text.strip
    end

    def alternate_extensions(path)
      path.sub(/\.js$/i, '.ts').sub(/\.css$/i, '.scss')
    end

    def render(context)
      site = context.registers[:site]
      assets_dir = site.config['assets_dir'] || 'assets'
      text = Liquid::Template.parse(@text).render(context)
      asset_path = File.join(assets_dir, text)

      file_path = File.join(site.source, asset_path)

      # We can't check if the files exist in the destination directory
      # as liquid is run before Generator's. So check if there is a
      # file that looks similar in the assets directory
      if !File.file?(file_path) && !File.file?(alternate_extensions(file_path))
        alternate_text = alternate_extensions(text)
        raise "Could not find static file that matches: #{text}"
        + (alternate_text != text ? " or #{alternate_extensions}" : '')
      end

      '/' + asset_path
    end
  end
end

Liquid::Template.register_tag('static', Jekyll::StaticTag)
