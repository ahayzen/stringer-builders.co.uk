# SPDX-FileCopyrightText: 2020 Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

# frozen_string_literal: true

require 'net/http'

module Jekyll
  # {% font url name %}
  #
  # Eg {% font https://fonts.googleapis.com/css?family=Roboto&display=swap roboto %}
  #
  # Reads the font css from a given url (eg a fonts.google.com one)
  # It then downloads the woff and woff2 files locally as static files
  # and rewrites the css to use local urls. The name is used as the filename.
  #
  # Note this can only handle 1 woff and 1 woff2 file at a time.
  class FontTag < Liquid::Tag
    def initialize(tag_name, text, tokens)
      super
      @font_url, _, @font_name = text.strip.rpartition(' ')
    end

    def download_remote_css(url, userAgent)
      # Download given url
      uri = URI(url)

      # Set that we expect text/css and set the userAgent
      req = Net::HTTP::Get.new(uri)
      req['accept'] = 'text/css,*/*;q=0.1'
      req['User-Agent'] = userAgent

      # Start the download
      res = Net::HTTP.start(uri.hostname, uri.port, use_ssl: true) do |http|
        http.request(req)
      end

      # Repond with the content
      if res.is_a?(Net::HTTPSuccess)
        res.body
      else
        raise String.new << 'Failed to load font url: ' << url << ' Message: ' << res.message
      end
    end

    def make_local_css(font_url, assets_font_path, font_woff_name, font_woff2_name, _css_cache_path, use_cache)
      # Check if we are using cache, as this is then noop and CSSFontFile and FontFile will read from cache
      if use_cache
        ['', '', '']
      else
        # Using Firefox < 39 should give us WOFF rather than WOFF2
        # https://developer.mozilla.org/en-US/docs/Web/CSS/@font-face#Browser_compatibility
        remote_css = download_remote_css(@font_url,
                                         'Mozilla/5.0 (X11; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/35.0')

        # Using Firefox >= 39 should give us WOFF2
        # Note we use less than 44 so we only get latin and not other variants (due to unicode-range support)
        remote_css << "\n" << download_remote_css(@font_url,
                                                  'Mozilla/5.0 (X11; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/39.0')

        # Extract remote paths in css
        woff_match = /^.*url\((?<woff>\S*)\) format\('woff'\);$/.match(remote_css)
        raise 'Could not find woff format in url: ' << font_url unless woff_match

        woff2_match = /^.*url\((?<woff2>\S*)\) format\('woff2'\);$/.match(remote_css)
        raise 'Could not find woff2 format in url: ' << font_url unless woff2_match

        remote_woff_url = woff_match[:woff]
        remote_woff2_url = woff2_match[:woff2]

        # Replace file paths in css with local ones
        remote_css = remote_css.sub(/(?<=url\()(\S*)(?=\) format\('woff'\);)/,
                                    File.join('/', assets_font_path, font_woff_name))
        remote_css = remote_css.sub(/(?<=url\()(\S*)(?=\) format\('woff2'\);)/,
                                    File.join('/', assets_font_path, font_woff2_name))

        [remote_css, remote_woff_url, remote_woff2_url]
      end
    end

    def render(context)
      # Generate expected paths
      site = context.registers[:site]
      assets_dir = site.config['assets_dir'] || 'assets'
      cache_dir = File.join(site.cache_dir, 'liquid_font')

      assets_font_path = File.join(assets_dir, 'css', 'fonts')
      css_name = String.new << @font_name << '.css'
      font_woff_name = String.new << @font_name << '.woff'
      font_woff2_name = String.new << @font_name << '.woff2'

      # If all of these exist and caching is enabled, then we are safe to use cache
      cache_css_path = File.join(cache_dir, css_name)
      cache_woff_path = File.join(cache_dir, font_woff_name)
      cache_woff2_path = File.join(cache_dir, font_woff2_name)
      use_cache = (Cache.disk_cache_enabled and File.file?(cache_css_path) and File.file?(cache_woff_path) and File.file?(cache_woff2_path))

      # Download and cache css font, return the remote font urls to download
      css_content, remote_woff_url, remote_woff2_url = make_local_css(@font_url, assets_font_path,
                                                                      font_woff_name, font_woff2_name, cache_css_path, use_cache)

      # Add static files for CSS, WOFF, WOFF2 if they don't exist
      unless site.static_files.any? do |sfile|
               sfile.instance_of?(CSSFontFile) && (sfile.name == css_name) && (sfile.content == css_content) && (sfile.cache_file == cache_css_path)
             end
        site.static_files << CSSFontFile.new(site, assets_font_path, css_name, css_content, cache_css_path)
      end
      unless site.static_files.any? do |sfile|
               sfile.instance_of?(FontFile) && (sfile.name == font_woff_name) && (sfile.remote_url == remote_woff_url) && (sfile.cache_file == cache_woff_path)
             end
        site.static_files << FontFile.new(site, assets_font_path, font_woff_name, remote_woff_url,
                                          cache_woff_path)
      end
      unless site.static_files.any? do |sfile|
               sfile.instance_of?(FontFile) && (sfile.name == font_woff2_name) && (sfile.remote_url == remote_woff2_url) && (sfile.cache_file == cache_woff2_path)
             end
        site.static_files << FontFile.new(site, assets_font_path, font_woff2_name, remote_woff2_url,
                                          cache_woff2_path)
      end

      # Return link using our generated local css
      '<link rel="stylesheet" href="' + File.join('/', assets_font_path, css_name) + '" />'
    end
  end

  class CSSFontFile < StaticFile
    attr_reader :cache_file, :content

    def initialize(site, dir, name, content, cache_file)
      super(site, site.source, dir, name, nil)

      @cache_file = cache_file
      @content = content
    end

    def write(dest)
      # Path of the static file
      dest_path = File.join(dest, @dir, @name)

      # Ensure the target directory exists
      FileUtils.mkdir_p(File.dirname(dest_path))

      # If there is a cache read from it
      if Cache.disk_cache_enabled && File.file?(@cache_file)
        FileUtils.cp(@cache_file, dest_path)
      else
        # Write css content to file
        open dest_path, 'w' do |io|
          io.write @content
        end

        # Cache if caching is enabled
        if Cache.disk_cache_enabled
          FileUtils.mkdir_p(File.dirname(@cache_file))
          FileUtils.cp(dest_path, @cache_file)
        end
      end
    end
  end

  class FontFile < StaticFile
    attr_reader :cache_file, :remote_url

    def initialize(site, dir, name, remote_url, cache_file)
      super(site, site.source, dir, name, nil)

      @cache_file = cache_file
      @remote_url = remote_url
    end

    def download_remote_font(url, dest_path)
      # Download the remote font
      uri = URI(url)
      req = Net::HTTP::Get.new(uri)
      res = Net::HTTP.start(uri.hostname, uri.port, use_ssl: true) do |http|
        http.request(req)
      end

      # Try to save to the local file path
      if res.is_a?(Net::HTTPSuccess)
        open dest_path, 'w' do |io|
          io.write res.body
        end
      else
        raise 'Could not download remote font: ' << url
      end
    end

    def write(dest)
      # Path of the static file
      dest_path = File.join(dest, @dir, @name)

      # Ensure the target directory exists
      FileUtils.mkdir_p(File.dirname(dest_path))

      # If there is a cache read from it
      if Cache.disk_cache_enabled && File.file?(@cache_file)
        FileUtils.cp(@cache_file, dest_path)
      else
        # Download the remote font into the local path
        download_remote_font(@remote_url, dest_path)

        # Cache if caching is enabled
        if Cache.disk_cache_enabled
          FileUtils.mkdir_p(File.dirname(@cache_file))
          FileUtils.cp(dest_path, @cache_file)
        end
      end
    end
  end
end

Liquid::Template.register_tag('font', Jekyll::FontTag)
