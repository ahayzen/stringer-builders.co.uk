---
description: "Stringer Builders: Master builders of reputation, reliability and distinction for over 50 years, primarily operating in Welwyn Garden City, Knebworth, Hitchin and the surrounding area. Step with confidence into our world of improvement, refurbishment and renovation."
layout: home
redirect_from:
    - gallery.htm
    # Old sub gallery sections
    - bathrooms.htm
    - conservatories.htm
    - construction.htm
    - ecclesiastical.htm
    - extensions.htm
    - gardens.htm
    - kitchens.htm
    - living.htm
    - stairways.htm
    - wardrobes.htm
title: Home
title-tag: Stringer Builders of Welwyn Garden City, Hertfordshire
---

Stringer Builders have been master builders of reputation, reliability and distinction for over 50 years, primarily operating in Welwyn Garden City, Knebworth, Hitchin and the surrounding area. Step with confidence into our world of improvement, refurbishment and renovation.

We're proud to be a bit different from other builders. Please take a look at the work we have done and you'll find ideas and insights from some of the many specialist projects we have undertaken. Please [contact Stringer Builders]({% link contact.md %} "Link to contact Stringer Builders") and see how we can help with your project.
