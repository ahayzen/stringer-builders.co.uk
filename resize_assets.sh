#!/bin/bash

# SPDX-FileCopyrightText: 2020 Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: CC0-1.0

SCRIPT=$(realpath "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

# Resize source images to a maximum of 4000x4000
mogrify -auto-orient -resize "4000x4000>" -strip "$SCRIPTPATH/assets/images/bespoke_carpentry/*"
mogrify -auto-orient -resize "4000x4000>" -strip "$SCRIPTPATH/assets/images/building_work/*"
mogrify -auto-orient -resize "4000x4000>" -strip "$SCRIPTPATH/assets/images/design/*"
mogrify -auto-orient -resize "4000x4000>" -strip "$SCRIPTPATH/assets/images/exteriors/*"
mogrify -auto-orient -resize "4000x4000>" -strip "$SCRIPTPATH/assets/images/interiors/*"
mogrify -auto-orient -resize "4000x4000>" -strip "$SCRIPTPATH/assets/images/others/*"
