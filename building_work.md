---
layout: section
title: Building Work
images:
    - { name: "Structural concrete slab", src: images/building_work/basement_1.jpg }
    - { name: "Structural underpinning", src: images/building_work/basement_2.jpg }
    - { name: "Soakaway construction", src: images/building_work/digging_1.jpg }
    - { name: "Siberian larch cladding", src: images/building_work/extension_1.jpg }
    - { name: "Structural alterations and plastering", src: images/building_work/fireplace_1.jpg }
    - { name: "New lead valley", src: images/building_work/lead_1.jpg }
    - { name: "Lead chimney step flashing", src: images/building_work/lead_2.jpg }
    - { name: "Lead dormer top", src: images/building_work/lead_3.jpg }
    - { name: "Lead outlet", src: images/building_work/lead_4.jpg }
    - { name: "Flat roof rainwater outlet", src: images/building_work/lead_5.jpg }
    - { name: "Chimney removal and support", src: images/building_work/roof_1.jpg }
    - { name: "Structural roof repair", src: images/building_work/roof_2.jpg }
    - { name: "Room ready for plastering", src: images/building_work/roof_3.jpg }
    - { name: "Loft conversion supporting steel", src: images/building_work/roof_4.jpg }
    - { name: "Roof light stricture", src: images/building_work/roof_5.jpg }
    - { name: "Loft conversion supporting steel 2", src: images/building_work/roof_6.jpg }
    - { name: "Building site 1", src: images/building_work/steel_1.jpg }
    - { name: "Building site 2", src: images/building_work/steel_2.jpg }
    - { name: "Bi-fold window supporting steel", src: images/building_work/steel_3.jpg }
    - { name: "Building site 3", src: images/building_work/timber_1.jpg }
    - { name: "Hand cut hipped roof 1", src: images/building_work/timber_roof_1.jpg }
    - { name: "Hand cut hipped roof 2", src: images/building_work/timber_roof_2.jpg }
    - { name: "Wall opening", src: images/building_work/wall_1.jpg }
    - { name: "Bullnose corner detail", src: images/building_work/wall_2.jpg }
    - { name: "Structural alterations 1", src: images/building_work/wall_3.jpg }
    - { name: "Structural alterations 2", src: images/building_work/wall_4.jpg }
    - { name: "New door location", src: images/building_work/wall_5.jpg }
    - { name: "Structural alterations 3", src: images/building_work/wall_6.jpg }
    - { name: "New cast window sill", src: images/building_work/window_1.jpg }
    - { name: "Plastering square reveals", src: images/building_work/window_2.jpg }
    - { name: "Door repair", src: images/building_work/window_3.jpg }
---
# Building Work

Stringer Builders are fully aware that the building process can be a stressful time for the homeowner but would like to take pride in the fact that many of its customers have used them time and time again and Paul now finds himself working for their children as they have become homeowners!

With over 30 years of building experience behind him, Paul has a good knowledge of building regulations and has a very good working relationship with local authority Building Inspectors - he has even been employed by them to work on their own properties!

Where possible, Stringer Builders very much likes to support and use local tradespeople and companies, thereby helping and supporting the local economy and reducing its carbon footprint.

The number of additional qualified Trades people employed will vary from contract to contract. Largely because of Paul's 'hands on' approach to all projects, Paul is fully knowledgeable about the standards expected of other trades and is able to deliver work to the highest of standards.

He likes to say to customers that *'he works to a standard, not to a time line'*.

His customers trust in him and know that if an element of their building work demands more work to achieve a high quality end result then Paul will give it his time and attention.
