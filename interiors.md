---
layout: section
title: Interiors
images:
    - { name: "Architectural bathroom with freestanding bath 1", src: images/interiors/bathroom_1_1.jpg }
    - { name: "Architectural bathroom with freestanding bath 2", src: images/interiors/bathroom_1_2.jpg }
    - { name: "Architectural bathroom with freestanding bath 3", src: images/interiors/bathroom_1_3.jpg }
    - { name: "Architectural bathroom with freestanding bath 4", src: images/interiors/bathroom_1_4.jpg }
    - { name: "Contemporary bathroom 1", src: images/interiors/bathroom_2_1.jpg }
    - { name: "Contemporary bathroom 2", src: images/interiors/bathroom_2_2.jpg }
    - { name: "Contemporary bathroom 3", src: images/interiors/bathroom_2_3.jpg }
    - { name: "Contemporary bathroom 4", src: images/interiors/bathroom_2_4.jpg }
    - { name: "Luxury bathroom for listed building 1", src: images/interiors/bathroom_3_1.jpg }
    - { name: "Luxury bathroom for listed building 2", src: images/interiors/bathroom_3_2.jpg }
    - { name: "Luxury bathroom for listed building 3", src: images/interiors/bathroom_3_3.jpg }
    - { name: "Iroko cupboard doors", src: images/interiors/cupboard_1.jpg }
    - { name: "Baywindow seat", src: images/interiors/cupboard_2.jpg }
    - { name: "Bespoke peninsula island with bookcase 1", src: images/interiors/desk_1_1.jpg }
    - { name: "Bespoke peninsula island with bookcase 2", src: images/interiors/desk_1_2.jpg }
    - { name: "Floating desk with American black walnut curved top", src: images/interiors/desk_2.jpg }
    - { name: "Open plan kitchen for modern living 1", src: images/interiors/kitchen_1_1.jpg }
    - { name: "Open plan kitchen for modern living 2", src: images/interiors/kitchen_1_2.jpg }
    - { name: "Open plan kitchen for modern living 3", src: images/interiors/kitchen_1_3.jpg }
    - { name: "New kitchen with solid surface worktops", src: images/interiors/kitchen_2_1.jpg }
    - { name: "Bespoke radiator cover", src: images/interiors/radiator_1.jpg }
    - { name: "Bespoke units and floating shelves", src: images/interiors/shelves_1.jpg }
    - { name: "Bespoke fitted wardrobes 1", src: images/interiors/shelves_2_1.jpg }
    - { name: "Bespoke fitted wardrobes 2", src: images/interiors/shelves_2_2.jpg }
    - { name: "Bespoke fitted wardrobes 3", src: images/interiors/shelves_2_3.jpg }
    - { name: "Contemporary alcove bookcase", src: images/interiors/shelves_3.jpg }
    - { name: "Oak and painted contemporary under stairs storage 1", src: images/interiors/staircase_1_1.jpg }
    - { name: "Oak and painted contemporary under stairs storage 2", src: images/interiors/staircase_1_2.jpg }
    - { name: "Oak and painted contemporary under stairs storage 3", src: images/interiors/staircase_1_3.jpg }
    - { name: "Oak and painted contemporary under stairs storage 4", src: images/interiors/staircase_1_4.jpg }
    - { name: "New cherry open string staircase 1", src: images/interiors/staircase_2_1.jpg }
    - { name: "New cherry open string staircase 2", src: images/interiors/staircase_2_2.jpg }
    - { name: "Hallway refurbishment with replacement hand rail and spindles", src: images/interiors/staircase_3.jpg }
    - { name: "New open string staircase with oak continuous handrail", src: images/interiors/staircase_4_1.jpg }
    - { name: "New open string staircase with oak continuous handrail", src: images/interiors/staircase_4_2.jpg }
    - { name: "Bespoke oak newel spindles and fumed oak handrail", src: images/interiors/staircase_5_1.jpg }
    - { name: "Oak fitted pantry 1", src: images/interiors/store_cupboard_1.jpg }
    - { name: "Oak fitted pantry 2", src: images/interiors/store_cupboard_2.jpg }
    - { name: "Bespoke designed and fitted wardrobe", src: images/interiors/wardrobe_1.jpg }
    - { name: "Bespoke pull handles", src: images/interiors/wardrobe_2.jpg }
    - { name: "Replacement double glazed window", src: images/interiors/window_1.jpg }
    - { name: "Bespoke replacement double glazed window to match existing 1", src: images/interiors/window_2_1.jpg }
    - { name: "Bespoke replacement double glazed window to match existing 2", src: images/interiors/window_2_2.jpg }
    - { name: "New skirting and architrave machined to match original WGC pattern", src: images/interiors/wood_1.jpg }
    - { name: "Original WGC skirting and architrave samples", src: images/interiors/wood_2.jpg }
    - { name: "New picture rail machined to match original WGC pattern", src: images/interiors/wood_3.jpg }
    - { name: "New skirting machined to match original WGC pattern", src: images/interiors/wood_4.jpg }
    - { name: "Bespoke oak newel to customers design", src: images/interiors/wood_5.jpg }
    - { name: "Replacement window moulding", src: images/interiors/wood_6.jpg }
---
# Interiors

Stringer Builders excel at creating beautiful interiors with a keen eye for detail&hellip; "The Devil is in the Detail".

Many older properties have odd-sized rooms or alcoves and require space-maximising solutions and this is something we have been doing for many years, marrying up good use of joinery designed to emphasise the architecture of a room and property.

It is important to us to ensure the comfort of every home so that everything simply falls into place and a room has a coherent feel.

One of Paul's strengths is working with timber and, with a fully functioning workshop, Paul is able to demonstrate the beauty and versatility of wood.

He especially enjoys restoring old properties and reinstating original mouldings and fittings. Success in a refurbishment can be measured when it is difficult to tell what is original and what is new.

As you can see from the images, Stringer Builders have worked on many interesting interior refurbishments from individual rooms to complete refurbishments of whole houses.
