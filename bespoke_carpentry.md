---
layout: section
title: Bespoke Carpentry
images:
    - { name: "English oak presentation box 1", src: images/bespoke_carpentry/box_1.jpg }
    - { name: "English oak presentation box 2", src: images/bespoke_carpentry/box_2.jpg }
    - { name: "English oak presentation box 3", src: images/bespoke_carpentry/box_3.jpg }
    - { name: "Oak &amp; Sycamore gift box", src: images/bespoke_carpentry/box_black_1.jpg }
    - { name: "Inlaid oak gift box 1", src: images/bespoke_carpentry/box_compass_1.jpg }
    - { name: "Inlaid oak gift box 2", src: images/bespoke_carpentry/box_compass_2.jpg }
    - { name: "Inlaid oak gift box 3", src: images/bespoke_carpentry/box_compass_3.jpg }
    - { name: "Oak planters", src: images/bespoke_carpentry/boxes_1.jpg }
    - { name: "Oak topped shop counter", src: images/bespoke_carpentry/shop_1.jpg }
    - { name: "Walnut cornice detail", src: images/bespoke_carpentry/shop_2.jpg }
    - { name: "Oak desk / Bookcase detail 1", src: images/bespoke_carpentry/shop_3.jpg }
    - { name: "Oak desk / Bookcase detail 2", src: images/bespoke_carpentry/shop_4.jpg }
    - { name: "Oak topped counter detail", src: images/bespoke_carpentry/shop_5.jpg }
    - { name: "Oak desk / Bookcase", src: images/bespoke_carpentry/shop_6.jpg }
    - { name: "Painted desk 1", src: images/bespoke_carpentry/shop_7.jpg }
    - { name: "Painted desk 2", src: images/bespoke_carpentry/shop_8.jpg }
    - { name: "Painted desk detail", src: images/bespoke_carpentry/shop_9.jpg }
    - { name: "Oak desk", src: images/bespoke_carpentry/shop_10.jpg }
    - { name: "Scottish oak counter top 1", src: images/bespoke_carpentry/shop_11.jpg }
    - { name: "Scottish oak counter top 2", src: images/bespoke_carpentry/shop_12.jpg }
    - { name: "Oak drawer unit", src: images/bespoke_carpentry/shop_13.jpg }
    - { name: "Oak drawer unit detail", src: images/bespoke_carpentry/shop_14.jpg }
    - { name: "Coaster 1", src: images/bespoke_carpentry/coaster_1.jpg }
    - { name: "Coaster 2", src: images/bespoke_carpentry/coaster_2.jpg }
    - { name: "Cherry chest of drawers", src: images/bespoke_carpentry/drawers_1.jpg }
    - { name: "Quarter sawn oak chest of drawers", src: images/bespoke_carpentry/drawers_2.png }
    - { name: "American black walnut console table", src: images/bespoke_carpentry/drawers_3.png }
    - { name: "Bead edge oak bedroom mirror", src: images/bespoke_carpentry/mirror_1.jpg }
    - { name: "Bead edge oak full length mirror", src: images/bespoke_carpentry/mirror_2.jpg }
    - { name: "Mirror frames", src: images/bespoke_carpentry/mirror_3.jpg }
    - { name: "Shaped picture rail", src: images/bespoke_carpentry/moulding_1.jpg }
    - { name: "Customer moulding sample", src: images/bespoke_carpentry/moulding_2.jpg }
    - { name: "Work during assembly", src: images/bespoke_carpentry/shelf_1.jpg }
    - { name: "Douglas fir pine cabinet", src: images/bespoke_carpentry/side_1.jpg }
    - { name: "Snowflake Christmas gift", src: images/bespoke_carpentry/snowflake.jpg }
    - { name: "Small mahogany table", src: images/bespoke_carpentry/table_1.jpg }
    - { name: "Coasters and mug", src: images/bespoke_carpentry/coasters_and_mug.jpg }
---
# Bespoke Carpentry

Stringer Builders have a permanent, well-equipped workshop in Welwyn Garden City, from which we can produce individually-designed fine quality cabinets and joinery for kitchens and bathrooms etc. We are largely technically-based builders with a substantial investment in tools and equipment enabling us to keep costs down and efficiency up.

Over the years Paul has worked on many interesting and exciting projects. Paul designed and built the counters, display units, paper racks and bookcase for a high end stationery shop in Covent Garden, London. They were all made from oak and walnut, the main counter being made from one very large piece of Scottish oak.

Paul has also made a number of bespoke English oak presentation boxes for the Historic Royal Palaces. He enjoys working with timber and making 'one off' pieces of furniture for clients and making unique gifts for friends and family.

Please [contact Paul Stringer]({% link contact.md %} "Contact Paul Stringer") if you are interested in having something very special made.
