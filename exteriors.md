---
layout: section
title: Exteriors
images:
    - { name: "Purposed made and designed front door in oak", src: images/exteriors/door_1.jpg }
    - { name: "Purpose made front door to match Welwyn Garden City original", src: images/exteriors/door_2.jpg }
    - { name: "Purpose made front door to match existing", src: images/exteriors/door_3.jpg }
    - { name: "Purpose made front door based on original Welwyn Garden City design", src: images/exteriors/door_4_1.jpg }
    - { name: "Door case repairs", src: images/exteriors/door_4_2.jpg }
    - { name: "Timber framed extension 1", src: images/exteriors/extension_1_1.png }
    - { name: "Timber framed extension 2", src: images/exteriors/extension_1_2.jpg }
    - { name: "Timber framed extension 3", src: images/exteriors/extension_1_3.jpg }
    - { name: "Design and build project for listed building", src: images/exteriors/extension_2_1.jpg }
    - { name: "Lead fabrication", src: images/exteriors/lead_1_1.jpg }
    - { name: "Lead corner detail", src: images/exteriors/lead_1_2.jpg }
    - { name: "Flat roof rainwater outlet", src: images/exteriors/lead_2_1.jpg }
    - { name: "Handmade lead hopper", src: images/exteriors/lead_2_2.jpg }
    - { name: "Flat roof rainwater outlet with handmade lead hopper", src: images/exteriors/lead_2_3.jpg }
    - { name: "Design and build garden room project 1", src: images/exteriors/out_building_1.jpg }
    - { name: "Design and build garden room project 2", src: images/exteriors/out_building_2.jpg }
    - { name: "Design and build garden room project 3", src: images/exteriors/out_building_3.jpg }
    - { name: "Design and build garden room project 4", src: images/exteriors/out_building_4.jpg }
    - { name: "Design and build garden room project 5", src: images/exteriors/out_building_5.jpg }
    - { name: "Rotten soffit repair", src: images/exteriors/roof_1.jpg }
    - { name: "Lead valley replacement 1", src: images/exteriors/roof_lead_1.jpg }
    - { name: "Lead valley replacement 2", src: images/exteriors/roof_lead_2.jpg }
    - { name: "New garden retaining wall 1", src: images/exteriors/wall_steps_1.jpg }
    - { name: "New garden retaining wall 2", src: images/exteriors/wall_steps_2.jpg }
    - { name: "New garden retaining wall 3", src: images/exteriors/wall_steps_3.jpg }
    - { name: "Replacement double glazed window to match existing", src: images/exteriors/window_1.jpg }
    - { name: "Conservation roof window installation", src: images/exteriors/window_2.jpg }
    - { name: "Bay window sill repair", src: images/exteriors/window_3.jpg }
    - { name: "Green oak screen 1", src: images/exteriors/wooden_frame_1.jpg }
    - { name: "Green oak screen 2", src: images/exteriors/wooden_frame_2.jpg }
    - { name: "Green oak screen 3", src: images/exteriors/wooden_frame_3.jpg }
    - { name: "Green oak screen 4", src: images/exteriors/wooden_frame_4.jpg }
    - { name: "Green oak screen 5", src: images/exteriors/wooden_frame_5.jpg }
    - { name: "Green oak screen 6", src: images/exteriors/wooden_frame_6.jpg }
    - { name: "Green oak screen 7", src: images/exteriors/wooden_frame_7.jpg }
    - { name: "Green oak screen 8", src: images/exteriors/wooden_frame_8.jpg }
---
# Exteriors

If space is limited inside your home and you are looking for something a little bit different, Stringer Builders have created some interesting and useful outdoor rooms.

Working through the ideas you may have and also coming up with our own innovative ideas to maximise the space available, it is possible to create useful spaces for the whole family; whether it might be an office, teenage den, garden room or a bespoke greenhouse and potting shed for the keen gardener to enjoy.

A green oak pergola, giving height and screening is a worthy investment that will add a mix of classic and contemporary styles to the finished project.

Stringer Builders are able to offer the complete package from design concept through to completion, including lead work, joinery, brickwork and advice on outdoor lighting.
