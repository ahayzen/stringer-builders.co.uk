---
layout: contact
image:
    name: Character
    src: images/layout/character.jpg
redirect_from:
    - contact.htm
title: Contact
---
# Contact

## Paul Stringer

### Telephone

<a data-area="7976" data-bind="link-ext-t" data-local="969388" href="#"></a>

### Email

<a data-bind="link-ext-m" data-domain="stringer-builders" data-name="paul" data-tld="co.uk" href="#"></a>

### Address

1 Longcroft Lane, Welwyn Garden City, Hertfordshire, AL8 6EB
