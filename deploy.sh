#!/bin/bash

# SPDX-FileCopyrightText: 2020, 2021 Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: LicenseRef-StringerBuildersProprietary

SCRIPT=$(realpath "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

# Clean build
rm -rf "$SCRIPTPATH/_site"

# Production build
export JEKYLL_ENV=production
bundle exec jekyll build

# Pause so that user can check files before upload
read -p "Press Enter to upload."

# Deploy to the site#
#
# For now we'll use ftp instead of rsync or sftp
# rsync -aP --delete "$SCRIPTPATH/_site/" stringer-builders.co.uk@ftp.stringer-builders.co.uk:/htdocs
lftp -f "
open ftp://stringer-builders.co.uk@ftp.stringer-builders.co.uk
mirror --delete --exclude=cgi-bin --no-perms --reverse --verbose '$SCRIPTPATH/_site/' /htdocs
bye
"
