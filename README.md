<!--
SPDX-FileCopyrightText: 2020 Andrew Hayzen <ahayzen@gmail.com>

SPDX-License-Identifier: CC0-1.0
-->
Jekyll Static Site for stringer-builders.co.uk
