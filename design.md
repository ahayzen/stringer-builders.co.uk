---
layout: section
title: Design
images:
    - { name: "Bed design", src: images/design/bed_1.png }
    - { name: "Pine cabinet", src: images/design/cabinet_1.jpg }
    - { name: "Front elevation", src: images/design/elevation_1.png }
    - { name: "Section drawing", src: images/design/footing_detail_1.png }
    - { name: "3D bathroom visualisation", src: images/design/mirror_1.jpg }
    - { name: "Refurbishment plan drawing", src: images/design/plan_1.png }
    - { name: "3D visualisation", src: images/design/plan_2.png }
    - { name: "Wardrobe 1", src: images/design/wardrobe_1.png }
    - { name: "Wardrobe 2", src: images/design/wardrobe_2.png }
---
# Design

Good design is the key to the successful outcome of any project. All projects are very much given our individual attention; working closely with our clients we feel is vitally important for the success of the project and because we are a small business it is possible for clients to change and adapt the project if required.

We are happy to give advice and guidance, or offer alternative suggestions to find the best solution to problems you may have in your property.

When designing we believe that looking at the whole picture can help unlock a rooms potential. It may be by altering the internal walls can vastly improve the 'flow' of a property.

Stringer Builders have worked on many Listed Buildings and have experience dealing with Conservation Officers. Rather than seeing them as a hindrance, we have always found it important to work alongside them and 'have them on board' with every project.

We are able to provide you with detailed scaled drawings, to help you visualise the final outcome of your design.

We like to think we bring an intelligent approach to building problems and much of the feedback over the last 50 years has led us to believe there is more than a little truth in this.

Please [contact Paul Stringer]({% link contact.md %} "Link to contact Paul Stringer") to see help you maximise your property's potential.
