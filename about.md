---
layout: default
redirect_from:
    - philosophy.htm
images:
    about:
        - { name: "David Stringer 1972", src: images/others/david_stringer_1972.jpg }
        - { name: "Paul Stringer 1973", src: images/others/paul_stringer_1973.jpg }
        - { name: "Cottages at Ayot early 1970s", src: images/others/cottages_at_ayot_early_1970s.jpg }
        - { name: "Building 1973", src: images/others/building_1973.jpg }
    history:
        - { name: "Brick and mugs", src: images/others/brick_and_mugs.jpg }
        - { name: "New skirting and architrave machined to match original WGC pattern", src: images/interiors/wood_1.jpg }
        - { name: "Original Welwyn Garden City brick collection", src: images/others/wgc_bricks.jpg }
        - { name: "Original WGC skirting and architrave samples", src: images/interiors/wood_2.jpg }
        - { name: "Door handle", src: images/others/door_handle.jpg }
title: About
---
# About

{% image images/layout/logo.png original alt="Logo" class="right" %}

Stringer Builders was established in 1967 by David Stringer setting out to offer home owners a high standard of specialist building services.

From the beginning Stringer Builders promoted a philosophy of high quality work with attention to detail. This detail showed not only in the construction process but also in dealing with client's requirements to make the often intrusive nature of building more bearable.

Following the retirement of David Stringer, his son Paul Stringer took over the running of Stringer Builders specialising in the sympathetic and bespoke upgrading of the older building.

With over 30 years of building experience behind him Paul has built up a vast knowledge of construction methods to apply and use on all projects. Paul very much enjoys having a 'hands on' approach to all his work and particularly likes working on interesting and challenging projects.

Paul is good at listening to clients' requirements and making suggestions of his own. He is then able to use his skill of computer-aided drafting and design, to help clients visualise their ideas.

{% include imagegrid.html images=page.images.about class="-all-images" %}

# History

A little bit of history about Paul for those who are interested&hellip; Paul is the grandson of two of the early inhabitants of Welwyn Garden City, George Henry Stringer and John F Eccles.

Henry Stringer (he used his middle name) was born in Salford; he attended Manchester Grammar and Art School and trained as a lithographic artist. He moved into his new house in Longcroft Lane, Welwyn Garden City with his wife in 1931, where they went on to have three boys. Henry was an accomplished commercial artist until 1940 and worked on wartime training films for the War Office. He returned to commercial art in the 1950s and was a life member of the London Sketch Club. He produced a series of posters for British Rail which can be seen at the York Railway Museum.

In 1929 John F Eccles was appointment financial secretary of Welwyn Garden City Ltd and played a major role in the reorganising and restructuring of its finances, steering the company through the difficult times of the depression and ensuring the solvency of the company, in 1936 he became General Manager of Welwyn Garden City Ltd.

He also ensured the profitability of Welwyn Department Store, later to be bought by the John Lewis Partnership in 1983, in 1945 he was rewarded with a seat on the board and became managing director of the Company and Store and later Managing Director of the Howardsgate Trust.

Paul has brought up his family in the same house as his grandfather, continuing the unbroken family connection for nearly 100 years.

The family's long association with Welwyn Garden City has driven Paul's interest in the original building materials used, the architectural styles and period details of the original houses in the area. Paul has built up a library of original mouldings and period details, including picture rails, skirting boards, architraves, door furniture and designs. This valuable resource has enabled him to reinstate features in customer's houses that have been long lost.

Paul is always interested in expanding this resource, so please do [contact Paul Stringer]({% link contact.md %} "Link to contact Paul Stringer") if you have an original feature that is worth conserving.

{% include imagegrid.html images=page.images.history class="-all-images" %}

# Copyright

All the photos on the website are of my own work unless stated otherwise.
